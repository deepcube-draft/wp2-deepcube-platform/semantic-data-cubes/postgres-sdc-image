FROM postgres:12
RUN apt-get update
RUN apt-get install -y build-essential python2-dev python3-dev python3-pip
RUN apt-get autoclean
RUN pip3 install pgxnclient
RUN apt-get install -y postgresql-server-dev-12
RUN pgxn install multicorn
RUN pip3 install xarray
RUN pip3 install zarr
RUN pip3 install fsspec
RUN pip install pyproj
RUN apt-get -y install nano
RUN apt-get -y install lsb-release
RUN apt-get -y install wget
RUN apt-get -y install gnupg2
RUN sh -c 'wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -'
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | tee /etc/apt/sources.list.d/pgdg.list'
RUN apt-get update
RUN apt-get -y install postgis postgresql-12-postgis-3
RUN apt-get autoclean
 
EXPOSE 5432
